import stringUtils from "mainam-react-native-string-utils";
const fs = require("fs");
const CONFIG_PATH = "./files/";
export default {
  saveFile: (host = "*", url = "", content) => {
    return new Promise((resolve, reject) => {
      try {
        host = host.toMd5();
        url = url.toMd5();
        const folder = CONFIG_PATH + host + "/";
        if (!fs.existsSync(folder)) {
          fs.mkdirSync(folder, { recursive: true });
        }
        fs.writeFile(folder + url + ".json", content, function (err) {
          if (err) {
            reject(err);
          }
          resolve(`${host} - ${host}`);
        });
      } catch (error) {
        console.log(error);
        reject(err);
      }
    });
  },
  readFile: (host = "*", url = "") => {
    return new Promise((resolve, reject) => {
      try {
        host = host.toMd5();
        url = url.toMd5();
        const folder = CONFIG_PATH + host + "/";
        if (fs.existsSync(folder + url + ".json"))
          fs.readFile(
            folder + url + ".json",
            { encoding: "utf-8" },
            (err, data) => {
              if (!err) {
                resolve(data);
              } else {
                reject(err);
              }
            }
          );
        else reject();
      } catch (error) {
        resolve(error);
      }
    });
  },
};
