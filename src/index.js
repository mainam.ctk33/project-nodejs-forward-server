import fileUtils from "../utils/file-utils";
import { isEmpty, cloneDeep } from "lodash";
import axios from "axios";
const FormData = require("form-data");
const cors = require("cors");
const express = require("express");
const fileUpload = require("express-fileupload");
const Jimp = require("jimp");
const queryString = require("query-string");
const { XMLParser, XMLBuilder, XMLValidator } = require("fast-xml-parser");
const {
  createProxyMiddleware,
  responseInterceptor,
} = require("http-proxy-middleware");
const TIMEOUT = 5 * 1000;
const responseJSON = (res, status, data = "") => {
  res.writeHead(status, {
    "Content-Type": "application/json",
    "Content-Length": Buffer.byteLength(data, ["utf8"]),
  });
  res.write(data, "utf8");
  res.end("");
};
const responseError = (res, status, data) => {
  res.writeHead(status, {
    "Content-Type": "text/plain",
  });
  res.write(data, "utf8");
  res.end("");
};

const client = axios.create({
  headers: {
    "Content-Type": "application/json",
  },
});
module.exports = {
  init(app) {
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    app.use(cors());
    app.use(fileUpload());
    app.use(
      "*",
      createProxyMiddleware({
        target: "https://api-sakura-test.isofh.vn",
        // pathRewrite: (path, req) => {
        //   if (path.startsWith("/8000")) path = path.replace("/8000", "");
        //   if (path.startsWith("/8095")) path = path.replace("/8095", "");
        //   return path;
        // },
        router: (req) => {
          // if (req.baseUrl.startsWith("/8000")) {
          //   return "http://10.0.0.98:8000";
          // }
          // if (req.baseUrl.startsWith("/8095")) {
          //   return "http://10.0.0.98:8095";
          // }
          req.header.host = "https://api-sakura-test.isofh.vn";
          return "https://api-sakura-test.isofh.vn";
        },
        proxyTimeout: TIMEOUT,
        timeout: TIMEOUT,
        secure: false,
        changeOrigin: true, // for vhosted sites, changes host header to match to target's host
        selfHandleResponse: true, // manually call res.end(); IMPORTANT: res.end() is called internally by responseInterceptor()

        onError: async (err, req, res) => {
          try {
            const url = req.url;
            const host = req.header.host;
            fileUtils
              .readFile(host, url)
              .then((content) => {
                if (content) {
                  return responseJSON(res, 200, content);
                } else {
                  return responseError(res, 503, content);
                }
              })
              .catch((e) => {
                return responseError(res, 503, e?.message);
              });
          } catch (error) {
            return responseError(res, 503, error?.message);
          }
        },
        onProxyReq(proxyReq, req, res) {
          const contentType = proxyReq.getHeader("Content-Type");
          let bodyData;

          if (contentType === "application/json") {
            bodyData = JSON.stringify(req.body);
          }

          if (contentType === "application/x-www-form-urlencoded") {
            bodyData = queryString.stringify(req.body);
          }

          if (bodyData) {
            proxyReq.setHeader("Content-Length", Buffer.byteLength(bodyData));
            proxyReq.write(bodyData);
          } else {
            proxyReq.end();
          }
        },
        onProxyRes: responseInterceptor(
          async (responseBuffer, proxyRes, req, res) => {
            const imageTypes = [
              "image/png",
              "image/jpg",
              "image/jpeg",
              "image/gif",
            ];
            // detect image responses
            if (imageTypes.includes(proxyRes.headers["content-type"])) {
              try {
                const image = await Jimp.read(responseBuffer);
                image.flip(true, false).sepia().pixelate(5);
                return image.getBufferAsync(Jimp.AUTO);
              } catch (err) {
                console.log("image processing error: ", err);
                return responseBuffer;
              }
            }
            try {
              const response = responseBuffer.toString("utf8"); // convert buffer to string
              const url = req.url;
              const host = req.header.host;
              if (res.statusCode == 200) {
                try {
                  const x = JSON.parse(response);
                  if (x.code == 0) {
                    await fileUtils.saveFile(host, url, response);
                  } else {
                    const content = await fileUtils.readFile(host, url);
                    if (content) return responseJSON(res, 200, content);
                  }
                } catch (error) {
                  try {
                    const parser = new XMLParser();
                    let jObj = parser.parse(response);
                    if (jObj.ResponseMsg.code == 0) {
                      await fileUtils.saveFile(
                        host,
                        url,
                        JSON.stringify(jObj.ResponseMsg)
                      );
                      return responseJSON(
                        res,
                        200,
                        JSON.stringify(jObj.ResponseMsg)
                      );
                    } else {
                      const content = await fileUtils.readFile(host, url);
                      if (content) return responseJSON(res, 200, content);
                    }
                  } catch (error) {}
                }
              } else {
                debugger;
                const content = await fileUtils.readFile(host, url);
                if (content) return responseJSON(res, 200, content);
              }
            } catch (error) {
              console.log(error);
            }
            return responseBuffer;
          }
        ),
      })
    );
  },
};
