require("@babel/register")({})
const express = require('express');
const backend = require('./src');

async function start() {
  const server = express();
  backend.init(server);
  server.listen(80, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:80`);
  });
}
start();
